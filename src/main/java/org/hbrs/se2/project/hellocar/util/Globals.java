package org.hbrs.se2.project.hellocar.util;

public class Globals {
    public static String CURRENT_USER = "current_User";

    public static class Pages {
        public static final String SHOW_CARS = "show";
        public static final String ENTER_CAR = "enter";

        public static final String LOGIN_VIEW = "login";
        public static final String MAIN_VIEW = "";

        public static final String REGISTER_VIEW = "register";
        public static final String REGISTER_STUDENT_VIEW = "registerstudent";
        public static final String REGISTER_COMPANY_VIEW = "registercompany";
    }

    public static class Roles {
        public static final String ADMIN = "admin";
        public static final String USER = "user";

        public static final String STUDENT = "student";
        public static final String COMPANY = "company";
    }

    public static class Errors {
        public static final String NOUSERFOUND = "nouser";
        public static final String SQLERROR = "sql";
        public static final String DATABASE = "database";
    }

}
